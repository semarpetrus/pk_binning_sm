assembly = config["assembly"]
prefix = config["prefix"]
outdir = config["outdir"]
index = config["index"]
threads = config["threads"]
threshold = config["threshold"]
org_str = 'infA lent m13 cov2 t4 t7 athai bsub ecol sent scer cfre paer vnat unidentified'
org_list = org_str.split()

opts = config.get("opts", {})

rule run_pk_binning:
    input:
        [assembly] + [f"{index}/kmers_{i}.b.gz" for i in range(8)] + [f"{index}/scores_{i}.b.gz" for i in range(8)] + [f"{index}/metadata.txt"]
    output:
        html = f"{outdir}/{prefix}_ref_cov.html",
        fastas = [f"{outdir}/{prefix}_{fasta}.fasta" for fasta in org_list]
    conda:
        "requirements.yml"
    container:
        "docker://semarpetrus/pankmer:pk_binning"
    params:
        assembly = assembly,
        prefix = prefix,
        outdir = outdir,
        index = index,
        threads = threads,
        threshold = threshold
    shell:
        'pk_binning -a {assembly} -p {prefix} -o {outdir} -i {index} -c {threads} -x {threshold};'
        'for i in {output.fastas}; do touch $i; done'
